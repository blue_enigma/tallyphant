package com.ciarang.tallyphant;


import android.os.AsyncTask;

import java.util.HashMap;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.io.IOException;

class MsgSender extends AsyncTask<HashMap, Integer, String> {

    @Override
    protected String doInBackground(HashMap... params) {
        //Retrieve params
        HashMap<String, Object> p = params[0];
        String testServer = (String) p.get("ip");
        String udpMsg = (String) p.get("msg");
        Integer port = (Integer) p.get("port");

        InetAddress serverAddr = null;
        DatagramSocket ds = null;
        DatagramPacket dp = null;
    /*
    if (android.os.Build.VERSION.SDK_INT > 9)
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }*/

        try {
            serverAddr = InetAddress.getByName(testServer);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        try {
            ds = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }

        dp = new DatagramPacket(udpMsg.getBytes(), udpMsg.length(), serverAddr, port);

        try {
            ds.send(dp);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ds.close();
        return null;
    }
}
